CFLAGS  = -pipe -march=native -funroll-loops -O3 -flto -std=c99
# CFLAGS += -fprofile-instr-generate -fcoverage-mapping
CFLAGS += -DNDEBUG
# CFLAGS += -g
LFLAGS += $(CFLAGS)

# 5.1 5.2 5.3 5.4 jit
LUA_VERSION = jit

# Enables multithreading
# GCC seems to have issues when OpenMP is enabled
ifeq ($(OPENMP),)
OPENMP   = $(shell test $(CC) = clang && echo 1 || echo 0)
endif

# libavif
AVIF = 1

# custom
FARBFELD = 1

# giflib
GIF = 1

# libheif, also handles AVIF unless libavif is also enabled
HEIF = 1

# libjpeg-turbo
JPG = 1

# libpng
PNG = 1

# custom
QOI = 1

# libwebp
WEBP = 1

# libfreetype2, libfontconfig
FREETYPE = 1

# Max for any single image. Default is 512M
LIL_MAX_IMAGE = $(shell numfmt --from=iec 512M)

CFLAGS += -DLIL_IMG_COMPRESSION_QUALITY=1.0f
CFLAGS += -DLIL_IMG_COMPRESSION_SPEED=0.8f

LUAOPEN_LIL = luaopen_lil

