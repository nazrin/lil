```
| Method     | Channels | Notes |
| ---        | ---      | ---   |
| alpha      | RGBA     |       |
| replace    | RGBA     |       |
| add        | RGB      |       |
| multiply   | RGB      |       |
| subtract   | RGB      |       |
| lighten    | RGB      |       |
| darken     | RGB      |       |
| average    | RGBA     |       |
| screen     | RGB      |       |
| dissolve   | RGB      |       |
| difference | RGB      |       |
| xor        | RGB      |       |
| and        | RGB      |       |
| or         | RGB      |       |
```
