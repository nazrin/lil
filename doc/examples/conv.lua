#!/bin/env luajit
local lil = require("lil")

if arg[1] == "--help" then
	return io.stderr:write("Usage:\n\tconv.lua IN OUT\n")
end

local inpath  = assert(arg[1])
local outpath = assert(arg[2])

lil.open(inpath):save(outpath)

