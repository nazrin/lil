return function(lil)
	lil.formatHandlers.farbfeldBzip2 = {
		import = function(compressed)
			local tmp = os.tmpname()
			io.popen(("bunzip2 -c > %q"):format(tmp), "w"):write(compressed):close() -- Decompress to a tmp file
			local data = io.open(tmp):read("*a") -- Read tmp file
			io.open(tmp, "w"):close() -- Clear tmp file
			return lil.formatHandlers.farbfeld.import(data)
		end,
		export = function(img)
			local data = lil.formatHandlers.farbfeld.export(img)
			local tmp = os.tmpname()
			io.popen(("bzip2 -c > %q"):format(tmp), "w"):write(data):close() -- Compress to a tmp file
			local compressed = io.open(tmp):read("*a") -- Read tmp file
			io.open(tmp, "w"):close() -- Clear tmp file
			return compressed
		end,
		pattern = "^BZh",
		extensions = { "%.ff%.bz2$", "%.farbfeld%.bz2$" }
	}
end

