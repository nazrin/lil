local lil = require("lil")

local size = 512
local bg = "044fd4"
local tx = "ff004b"

local function getRingPos(rad, i, n)
	local ang = (i / n) * 2 * math.pi
	local x = size/2 + (rad * math.cos(ang))
	local y = size/2 - (rad * math.sin(ang))
	return x, y
end

local canvas = lil.new(size, size)

canvas:circle(bg, size/2, size/2, (size / 2) - (size/500))

for i=1,8 do
	local num, rad, s = i * 3, i * (size / 19), i * (size / 200)
	for o=1,num do
		local x, y = getRingPos(rad, o, num)
		canvas:circle("000a", x, y, s)
	end
end

local font = lil.font("SourceCodePro:semibold", tx, size / 3)
local text = font:text("LIL")
canvas:comp(text, size / 16, size / 2 - text.h / 2.1, { blend = "darken" })
canvas:comp(text, size / 20, size / 2 - text.h / 2)

canvas:resize(128):save("doc/html/data/logo.png")

