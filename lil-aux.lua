-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.
---
-- @module Lil
-- @license MPL-2.0
-- @author nazrin

return function(lil)

	local function findHandler(data, path)
		if data then
			for _,handler in pairs(lil.formatHandlers) do
				if handler.pattern and data:find(handler.pattern) then
					return handler
				end
			end
		end
		if path then
			for _,handler in pairs(lil.formatHandlers) do
				for _,ext in pairs(handler.extensions) do
					if path:find(ext) then
						return handler
					end
				end
			end
		end
	end

	---
	-- @function import
	-- @string data
	-- @return Img
	function lil.import(data, opts)
		local handler = assert(findHandler(data))
		local img = assert(handler.import(data, opts))
		return img
	end
	---
	-- @function open
	-- @string path
	-- @return Img
	function lil.open(path, opts)
		local file = assert(io.open(assert(path, "No path"), "r"))
		local data = assert(file:read("*a"))
		file:close()
		local handler = assert(findHandler(data, path), ("No handler found for %s"):format(path))
		local img = assert(handler.import(data, opts))
		img.path = path
		return img
	end

	--- @type Img

	---
	-- @function Img:save
	-- @string path
	-- @tab[opt={}] opts
	-- @float[opt=1.0] opts.quality
	-- @float[opt=0.8] opts.speed
	function lil.imgMT:save(path, opts)
		local handler = assert(findHandler(nil, assert(path, "No path")), ("No handler found for %s"):format(path))
		local data = assert(handler.export(self, opts))
		local file = assert(io.open(path, "w"))
		file:write(data)
		file:close()
		return self
	end
	---
	-- @function Img:export
	-- @string format
	-- @tab[opt={}] opts
	-- @float[opt=1.0] opts.quality
	-- @float[opt=0.8] opts.speed
	-- @return string
	function lil.imgMT:export(typ, opts)
		local handler = assert(findHandler(nil, typ), ("No handler found for %s"):format(typ))
		local data = assert(handler.export(self, opts))
		return data
	end
	--- Creates a new blank image with the same dimensions
	-- @function Img:new
	-- @colour[opt=transparent] colour
	-- @return img
	function lil.imgMT:new(colour)
		local img = lil.new(self.w, self.h)
		if colour then img:fill(colour) end
		return img
	end
	--- Swaps the internal structures with a new image. Used when a method has to create a new lil_Image to replace the current one. Anything that resizes the image or has to keep the full image intact while processing can create new blank template with `img:new` or `lil:new` and then swap the internals, after which the old `ud` gets garbage collected by Lua
	-- @function Img:overwriteInternals
	-- @param img
	-- @return nil
	function lil.imgMT:overwriteInternals(img2)
		self.ud, self.w, self.h = img2.ud, img2.w, img2.h
	end

	--- @section filters

	--- Flips horizontally
	-- @function Img:mirror
	-- @see Img:flip
	function lil.imgMT:mirror()
		return self:flip("h")
	end
	--- Composites the image on top of a colour
	-- @function Img:bg
	-- @colour colour
	function lil.imgMT:bg(col)
		local img2 = self:new(assert(col, "Need a colour")):comp(self)
		self:overwriteInternals(img2)
		return self
	end
	--- Overlays the colour on top of the image
	-- @function Img:fg
	-- @colour colour
	function lil.imgMT:fg(col)
		return self:rect(col, 0, 0, self.w, self.h)
	end

end

