dummy := $(shell test -f config.mk || cp config.def.mk config.mk)
include config.mk

PROGRAM = lil.so

LIL_VERSION = `TZ=UTC0 git show --no-patch --format='%ad_%h' --date=format-local:'%Y%m%d_%H%M%S'`

ifeq ($(LUA_INCDIR),)
ifeq ($(LUA_VERSION), jit)
CFLAGS += `pkg-config --cflags luajit || echo -I/usr/include/lua{,5.1}`
else ifeq ($(LUA_VERSION), 5.1)
CFLAGS += `pkg-config --cflags lua5.1 --silence-errors`
else ifeq ($(LUA_VERSION), 5.2)
CFLAGS += `pkg-config --cflags lua5.2 --silence-errors`
else ifeq ($(LUA_VERSION), 5.3)
CFLAGS += `pkg-config --cflags lua5.3 --silence-errors`
else ifeq ($(LUA_VERSION), 5.4)
CFLAGS += `pkg-config --cflags lua5.4 --silence-errors`
else
$(error Invalid Lua version $(LUA_VERSION))
endif
else
LFLAGS += $(LIBFLAG)
CFLAGS += -I$(LUA_INCDIR)
endif


ifeq ($(PNG), 1)
CFLAGS += `pkg-config --cflags libpng` -DLIL_USE_PNG
LFLAGS += `pkg-config --libs libpng`
endif

ifeq ($(JPG), 1)
CFLAGS += `pkg-config --cflags libturbojpeg` -DLIL_USE_JPEG
LFLAGS += `pkg-config --libs libturbojpeg`
endif

ifeq ($(GIF), 1)
CFLAGS += -DLIL_USE_GIF
LFLAGS += -lgif
endif

ifeq ($(WEBP), 1)
CFLAGS += `pkg-config --cflags libwebp` -DLIL_USE_WEBP
LFLAGS += `pkg-config --libs libwebp`
endif

ifeq ($(AVIF), 1)
CFLAGS += `pkg-config --cflags libavif` -DLIL_USE_AVIF
LFLAGS += `pkg-config --libs libavif`
endif

ifeq ($(HEIF), 1)
CFLAGS += `pkg-config --cflags libheif` -DLIL_USE_HEIF
LFLAGS += `pkg-config --libs libheif`
endif

ifeq ($(QOI), 1)
CFLAGS += -DLIL_USE_QOI
endif

ifeq ($(FARBFELD), 1)
CFLAGS += -DLIL_USE_FARBFELD
endif

ifeq ($(FREETYPE), 1)
CFLAGS += `pkg-config --cflags freetype2 fontconfig` -DLIL_USE_FREETYPE
LFLAGS += `pkg-config --libs freetype2 fontconfig`
endif

ifeq ($(OPENMP), 1)
CFLAGS += -fopenmp
else
CFLAGS += -Wno-unknown-pragmas
endif

CFLAGS += -DLIL_MAX_IMAGE=$(LIL_MAX_IMAGE)
CFLAGS += -DLIL_MAX_USAGE=$(LIL_MAX_USAGE)
CFLAGS += -Wall -Werror=pointer-arith -Wvla -fPIC
CFLAGS += -DLIL_VERSION=\"$(LIL_VERSION)\"

LFLAGS += -shared

SRC = $(shell find src/ -type f -name '*.c' ! -name 'amgl.c')
OBJ = $(SRC:.c=.o)

main: config.mk $(PROGRAM)

$(PROGRAM): $(OBJ)
	@echo $(LD) "	" $@
	@$(CC) $(OBJ) $(LFLAGS) -o $(PROGRAM)

%.o: %.c config.mk makefile aux_src.h
	@echo $(CC) "	" $@
	@$(CC) -c $(CFLAGS) $< -o $@

aux_src.h: txt2h.lua lil-aux.lua
	lua txt2h.lua aux_src < lil-aux.lua > aux_src.h

docs:
	@ldoc . -c doc/config.ld --icon logo.png >/dev/null

ut: $(PROGRAM)
	@luajit ./ut.lua

static: $(OBJ)
	@echo $(AR) "	" lil.a
	@$(AR) -r lil.a $(OBJ)

.PHONY: info
info:
	@echo CFLAGS: $(CFLAGS)
	@echo
	@echo LFLAGS: $(LFLAGS)

.PHONY: features
features:
	@echo features: OPENMP=$(OPENMP) PNG=$(PNG) JPG=$(JPG) GIF=$(GIF) WEBP=$(WEBP) AVIF=$(AVIF) FARBFELD=$(FARBFELD) FREETYPE=$(FREETYPE)

.PHONY: clean
clean:
	rm -fv -- $(OBJ) $(PROGRAM) aux_src.h

