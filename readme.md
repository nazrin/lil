# Lua Image Library

A comfortable image library for Lua

## General API

```lua
local lil = require("lil")

lil.open("img.png"):invert():save("inverted.png", { speed = 1.0 })

local font = lil.font("sans:italic:lang=is", "fff", 100)
local img = font:text("Vertu sæll og blessaður!")
img:expand(100):bg("000")
img:save("sæll.png")
```

Colours can be either a table of normalised floats `{ r, g, b[, a] }` or a hex string `RGB`, `RGBA`, `RRGGBB`, or `RRGGBBAA` with an optional `#` prefix

Coordinates are in `X,Y` order and start at 0 instead of the usual 1 in Lua

Documentation is available at [nazrin.codeberg.page/lil/@pages/doc/html/](https://nazrin.codeberg.page/lil/@pages/doc/html/) or can be generated using `make docs`

Examples are in `doc/examples/`

## Installing with Luarocks

Install: `luarocks install lil --lua-version=5.X [CC=clang]`

Link: [luarocks.org/modules/nazrin/lil](https://luarocks.org/modules/nazrin/lil)

## Building

There are no hard dependencies other than Lua 5.1+, GNU Make or Luarocks, and a (sanely modern) C99 compiler

Optional dependencies are OpenMP and the libraries listed below

GCC seems to have issues when OpenMP is enabled. The default configuration disables multithreading unless using clang: `CC=clang`

Required for the default configuration:
* Arch derived: `pacman -S giflib fontconfig libjpeg-turbo libwebp freetype2 luajit libpng libavif`
* Debian derived `apt install libpng-dev libgif-dev libfontconfig-dev libturbojpeg-dev libwebp-dev libfreetype-dev libavif-dev libluajit-5.1-dev`

### GNU Make

* Edit `config.mk` if you want
* `make` or `make amgl`

## Supported formats

### Import/Export

* avif (libavif or libheif)
* [farbfeld](https://tools.suckless.org/farbfeld/) (custom)
* heif (libheif)
* jpeg (libjpeg-turbo)
* png (libpng)
* [qoi](https://qoiformat.org/) (custom)
* webp (libwebp)

### Import

* gif (giflib)
* text rastering (libfreetype2, libfontconfig)

## Extending

See `src/img.h` for C extensions

`lil.ctxMT` and `lil.imgMT` can be added to for adding methods either from C or Lua

### Adding handlers

Handlers are stored in `lil.formatHandlers`, to extend it you can add an entry with the id (name) of the format as the index

An example that reads farbfeld files compressed with bzip2 is in `doc/examples/lil-ffbz2.lua`

```lua
lil.formatHandlers.png = {
	import     = function(data) … return Img end,
	export     = function(Img) … return data end,
	pattern    = "^\x89PNG\x0d\x0a\x1a\x0a",
	extensions = { '%.png$' }
}
```

