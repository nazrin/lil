rockspec_format = "3.0"
package = "LIL"
version = "master-1"
source = {
	url = "git+https://codeberg.org/nazrin/lil"
}
description = {
	summary = "A capable and lightweight image processing library for Lua designed to be comfortable to use",
	homepage = "https://codeberg.org/nazrin/lil",
	license = "MPL-2.0",
	issues_url = "https://codeberg.org/nazrin/lil/issues",
	labels = { "c", "image" }
}
dependencies = {
	"lua >= 5.1"
}
build = {
	type = "make",
	install = {
		lib = {
			["lil"] = "lil.so",
		},
	},
	build_variables = {
		LIBFLAG    = "$(LIBFLAG)",
		LUA_LIBDIR = "$(LUA_LIBDIR)",
		LUA_BINDIR = "$(LUA_BINDIR)",
		LUA_INCDIR = "$(LUA_INCDIR)",
		LUA        = "$(LUA)",
	},
	install_pass = false,
	platforms = {
		unix = {
			modules = {
				lil = {
					incdirs = { "/usr/include/freetype2" },
				}
			}
		}
	}
}

