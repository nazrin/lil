/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
///
// @module Lil
// @license MPL-2.0
// @author nazrin
#include <math.h>

#include "inc.h"

/// Creates a blank image
// @function new
// @int w
// @int[opt=w] h
// @return Img
static LUAFUNC(new){
	const int w = luaL_checkinteger(L, 1);
	const int h = luaL_optinteger(L, 2, w);
	luaL_argcheck(L, w >= 1, 2, "Width must be >= 1");
	luaL_argcheck(L, h >= 1, 3, "Height must be >= 1");
	const lil_Image* img = lil_newImageCalloc(L, w, h);
	if(!img)
		return 0;
	lil_pushImage(L, img);
	return 1;
}

/// Creates a new font
// @function font
// @string nameOrPath If the string includes a directory seperator (Linux/*BSD/Mac: `/`, Windows: `\\`) it's a path to a font file, otherwise it's a [FontConfig match string](https://man.archlinux.org/man/fc-match.1.en)
// @colour col
// @int w
// @int[opt=w] h
// @return Font
// @usage font("sans", "fff", 20)
// @usage font("DejaVuSans:bold", { 0, 0, 0, 1 }, 100)
// @usage font("monospace:italic", "ff00fa", 25)
// @usage font("serif:lang=ru:weight=bold", "00ff00fa", 95)
// @usage font("/usr/share/fonts/TTF/DejaVuSans.ttf", "00ff00fa", 95)
#ifdef LIL_USE_FREETYPE
static LUAFUNC(font){
	const char* nameOrPath = luaL_checkstring(L, 1);
	const lil_Colour col = lil_getColour(L, 2, NULL);
	const char* path = NULL;
	bool isPath = false;
	for(int i = 0; nameOrPath[i]; i++){
		#ifdef _WIN32
		#define CS '\\'
		#else
		#define CS '/'
		#endif
		if(nameOrPath[i] == CS){
			isPath = true;
			break;
		}
		#undef CS
	}
	if(isPath){
		path = nameOrPath;
	} else {
		path = lil_findFontPath(nameOrPath);
		if(!path)
			return luaL_error(L, "Failed to load font");
	}
	const int w = luaL_checkinteger(L, 3);
	const int h = luaL_optinteger(L, 4, w);
	luaL_argcheck(L, w > 1 && h > 1, 3, "Font size invalid");
	lil_Font* font = lil_newFont(path, w, h, col);
	if(!isPath)
		lil_free((char*)path);
	if(!font)
		return luaL_error(L, "Failed to load font");
	*(lil_Font**)lua_newuserdata(L, sizeof(lil_Font**)) = font;
	luaL_getmetatable(L, LIL_FONT_UD_MT);
	lua_setmetatable(L, -2);
	return 1;
}

///
// @function findFontPath
// @string FontConfig match string
// @return string path
static LUAFUNC(findFontPath){
	const char* name = luaL_checkstring(L, 1);
	char* path = lil_findFontPath(name);
	if(!path)
		return 0;
	lua_pushstring(L, path);
	lil_free(path);
	return 1;
}
#endif

lil_libstore LUAREG(lilLib) = {
	{ "new", LUAFUNCD(new) },
	#ifdef LIL_USE_FREETYPE
	{ "font", LUAFUNCD(font) },
	{ "findFontPath", LUAFUNCD(findFontPath) },
	#endif
	{ NULL, NULL }
};

