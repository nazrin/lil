/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

union lil_ColourLab{
	lil_Number arr[3];
	struct {
		lil_Number L, a, b;
	};
} typedef lil_ColourLab;
lil_ColourLab lil_rgb2Lab(lil_Colour col);

union lil_ColourHsv{
	lil_Number arr[3];
	struct {
		lil_Number h, s, v;
	};
} typedef lil_ColourHsv;
lil_ColourHsv lil_rgb2hsv(lil_Colour col);
lil_Colour    lil_hsv2rgb(lil_ColourHsv hsv);

