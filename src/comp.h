/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#define LIL_BM(id) LIL_BM ## id

enum lil_BlendMode{
	LIL_BM(ALPHA), LIL_BM(REPLACE),
	LIL_BM(ADD), LIL_BM(MULTIPLY), LIL_BM(SUBTRACT),
	LIL_BM(LIGHTEN), LIL_BM(DARKEN), LIL_BM(AVERAGE),
	LIL_BM(SCREEN), LIL_BM(DISSOLVE), LIL_BM(DIFFERENCE),
	LIL_BM(XOR), LIL_BM(AND), LIL_BM(OR),
};
enum lil_SampleMode{
	LIL_SMBILINEAR, LIL_SMBOX, LIL_SMNEAREST,
	LIL_BMTRANSPARENT = 1 << 8,
	LIL_BMBLACK       = 1 << 9,
	LIL_BMWHITE       = 1 << 10,
	LIL_BMMIRROR      = 1 << 11,
	LIL_BMEDGE        = 1 << 12,
};

__attribute__((hot)) void lil_composite(const enum lil_BlendMode cm, lil_Colour* b, const lil_Colour* t);

__attribute__((hot)) lil_Colour lil_sampleImage(const enum lil_SampleMode sm, const lil_Image* img, const lil_Number x, const lil_Number y, const lil_Number sx, const lil_Number sy);

extern const char* const lil_compMethodList[];
extern const char* const lil_sampleMethodList[];
extern const char* const lil_sampleMethodBoundsList[];

