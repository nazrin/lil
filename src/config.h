/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <lua.h>

#define LIL_MT         "org.codeberg.nazrin.lil"
#define LIL_IMG_MT     "org.codeberg.nazrin.lil.img"
#define LIL_CLEAN_MT   "org.codeberg.nazrin.lil.clean"
#define LIL_IMG_UD_MT  "org.codeberg.nazrin.lil.img.ud"
#define LIL_FONT_UD_MT "org.codeberg.nazrin.lil.font.ud"

#define LUAOPEN_LIL  luaopen_lil

typedef lua_Number lil_Number;

#ifndef LIL_IMG_COMPRESSION_QUALITY
#define LIL_IMG_COMPRESSION_QUALITY 1.0f
#endif
#ifndef LIL_IMG_COMPRESSION_SPEED
#define LIL_IMG_COMPRESSION_SPEED 0.8f
#endif

#ifndef LIL_MAX_IMAGE
#define LIL_MAX_IMAGE 0
#endif

#if LIL_MAX_IMAGE > 0
#define LIL_USE_LIMITS 1
#endif

#ifdef __cplusplus
#define lil_restrict __restrict__
#define lil_libstore extern
#define lil_externc extern "C"
#else
#define lil_restrict restrict
#define lil_libstore
#define lil_externc
#endif

