/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#ifdef LIL_USE_AVIF
#include <avif/avif.h>

#include "../inc.h"

// https://github.com/AOMediaCodec/libavif/blob/main/examples/avif_example_decode_memory.c
// https://github.com/AOMediaCodec/libavif/blob/main/examples/avif_example_encode.c

LUAFUNC(importAvif){
	size_t dataLen;
	const uint8_t* data = (const uint8_t*)lua_tolstring(L, 1, &dataLen);
	lil_Image* img;

	avifRGBImage rgb;
	avifResult r;
	memset(&rgb, 0, sizeof(rgb));

	avifDecoder* decoder = avifDecoderCreate();

	if((r = avifDecoderSetIOMemory(decoder, data, dataLen)) != AVIF_RESULT_OK)
		goto fail;
	if((r = avifDecoderParse(decoder)) != AVIF_RESULT_OK)
		goto fail;
	if((r = avifDecoderNextImage(decoder)) != AVIF_RESULT_OK)
		goto fail;

	img = lil_newImage(L, decoder->image->width, decoder->image->height);
	if(!img){
		avifDecoderDestroy(decoder);
		return 0;
	}

	avifRGBImageSetDefaults(&rgb, decoder->image);
	rgb.format = AVIF_RGB_FORMAT_RGBA;
	rgb.pixels = img->dptr;
	rgb.rowBytes = 4 * img->w;
	rgb.depth = 8;
	if((r = avifImageYUVToRGB(decoder->image, &rgb)) != AVIF_RESULT_OK)
		goto fail;

	lil_pushImage(L, img);
	avifDecoderDestroy(decoder);
	return 1;

	fail:
	avifDecoderDestroy(decoder);
	lua_pushnil(L);
	lua_pushstring(L, avifResultToString(r));
	return 2;
}

LUAFUNC(exportAvif){
	const lil_Image* img = lil_getImage(L, 1);

	avifRWData output = AVIF_DATA_EMPTY;
	avifRGBImage rgb;
	memset(&rgb, 0, sizeof(rgb));

	avifImage* aimg = avifImageCreate(img->w, img->h, 8, AVIF_PIXEL_FORMAT_YUV444);
	if(!aimg)
		return 0;

	avifRGBImageSetDefaults(&rgb, aimg);
	rgb.format = AVIF_RGB_FORMAT_RGBA;
	rgb.pixels = img->dptr;
	rgb.rowBytes = 4 * img->w;
	rgb.depth = 8;
	if(avifImageRGBToYUV(aimg, &rgb) != AVIF_RESULT_OK){
		avifImageDestroy(aimg);
		return 0;
	}

	avifEncoder* encoder = avifEncoderCreate();
	if(!encoder){
		avifImageDestroy(aimg);
		return 0;
	}

	lil_Number quality, speed;
	lil_getExportOpts(L, 2, &quality, &speed);
	bool lossLess = quality >= 0.9999 && quality <= 1.0001;
	encoder->quality = lossLess ? AVIF_QUALITY_LOSSLESS : quality * 100;
	encoder->speed = speed * 10;

	if(avifEncoderAddImage(encoder, aimg, 1, AVIF_ADD_IMAGE_FLAG_SINGLE)){
		avifImageDestroy(aimg);
		avifEncoderDestroy(encoder);
		return 0;
	}
	if(avifEncoderFinish(encoder, &output)){
		avifImageDestroy(aimg);
		avifEncoderDestroy(encoder);
		return 1;
	}

	lua_pushlstring(L, (char*)output.data, output.size);

	avifRWDataFree(&output);
	avifImageDestroy(aimg);
	avifEncoderDestroy(encoder);
	return 1;
}

#endif

