/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#ifdef LIL_USE_FARBFELD
#ifdef _WIN32
#include <winsock2.h>
#else
#include <arpa/inet.h>
#endif

#include "../inc.h"

LUAFUNC(importFarbfeld){
	size_t len;
	const char* data = lua_tolstring(L, 1, &len);
	if(len < 18)
		return 0;
	const uint32_t w = ntohl(*((uint32_t*)(data+8+0)));
	const uint32_t h = ntohl(*((uint32_t*)(data+8+4)));
	if(w + h < 2)
		return 0;
	if((w * h * 4 * 2) + 16 > len)
		return 0;
	const lil_Image* img = lil_newImage(L, w, h);
	if(!img)
		return 0;
	#pragma omp parallel for
	for(size_t o = 0; o < (size_t)img->w * img->h; o++){
		#define chan(i) img->d[o].arr[i] = data[((o*8)+((i)*2))+16]
		chan(0);
		chan(1);
		chan(2);
		chan(3);
		#undef chan
	}
	lil_pushImage(L, img);
	return 1;
}

LUAFUNC(exportFarbfeld){
	const lil_Image* img = lil_getImage(L, 1);
	const size_t len = 16 + (img->w * img->h * 4 * 2);
	uint8_t* data = (uint8_t*)lil_malloc(len);
	memcpy(data, "farbfeld", 8);
	*(uint32_t*)(data+8) = htonl(img->w);
	*(uint32_t*)(data+12) = htonl(img->h);
	uint8_t* dv = (uint8_t*)&(data[16]);
	#pragma omp parallel for
	for(size_t o = 0; o < (size_t)img->w * img->h; o++){
		if(img->d[o].a){
			#define chan(i) dv[(o*8)+((i)*2)+0] = dv[(o*8)+((i)*2)+1] = img->d[o].arr[i]
			chan(0);
			chan(1);
			chan(2);
			chan(3);
			#undef pix
		} else {
			#if INTPTR_MAX == INT64_MAX
			*((uint64_t*)&dv[o * 8]) = 0;
			#else
			*((uint32_t*)&dv[o * 8])       = 0;
			*((uint32_t*)&dv[(o * 8) + 4]) = 0;
			#endif
		}
	}
	lua_pushlstring(L, (const char*)data, len);
	lil_free(data);
	return 1;
}

#endif

