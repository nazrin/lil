/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#ifdef LIL_USE_HEIF
#include <libheif/heif.h>

#include "../inc.h"

// https://github.com/strukturag/libheif

LUAFUNC(importHeif){
	size_t dataLen;
	const uint8_t* data = (const uint8_t*)lua_tolstring(L, 1, &dataLen);

	lil_Image* img = NULL;
	struct heif_context* ctx = NULL;
	struct heif_image_handle* handle = NULL;
	struct heif_image* himg = NULL;
	struct heif_error err;
	const uint8_t* pixels;
	int w, h;

	memset(&err, 0, sizeof(err));
	if((ctx = heif_context_alloc()) == NULL)
		goto fail;
	if((err = heif_context_read_from_memory_without_copy(ctx, data, dataLen, NULL)).code)
		goto fail;
	if((err = heif_context_get_primary_image_handle(ctx, &handle)).code)
		goto fail;
	if((err = heif_decode_image(handle, &himg, heif_colorspace_RGB, heif_chroma_interleaved_RGBA, NULL)).code)
		goto fail;

	w = heif_image_get_width(himg, heif_channel_interleaved);
	h = heif_image_get_height(himg, heif_channel_interleaved);
	if(w <= 0 || h <= 0)
		goto fail;

	int stride;
	pixels = heif_image_get_plane_readonly(himg, heif_channel_interleaved, &stride);
	if(!pixels)
		goto fail;

	if(stride == w * 4){
		img = lil_newImage(L, w, h);
		if(!img)
			goto fail;
		memcpy(img->dptr, pixels, (size_t)w * h * 4);
	} else {
		img = lil_newImageCalloc(L, w, h);
		if(!img)
			goto fail;
		int s = min(stride, w * 4);
		#pragma omp parallel for
		for(int y = 0; y < h; y++)
			memcpy(&img->d[(size_t)y*w], &pixels[(size_t)y*stride], s);
	}

	heif_image_release(himg);
	heif_image_handle_release(handle);
	heif_context_free(ctx);

	lil_pushImage(L, img);
	return 1;

	fail:
	lua_pushnil(L);
	if(err.code)
		lua_pushstring(L, err.message);
	else
		lua_pushstring(L, "Error decoding HEIF");

	heif_image_release(himg);
	heif_image_handle_release(handle);
	heif_context_free(ctx);
	return 2;
}

static const char* const formatsStr[] = {
	"HEVC",
	"AVC",
	"JPEG",
	"AV1",
	"VVC",
	"EVC",
	"JPEG2000",     // ISO/IEC 15444-16:2021
	"uncompressed", // ISO/IEC 23001-17:2023
	NULL,
};

static struct heif_error myHeifWriter(struct heif_context* ctx, const void* data, size_t size, void* ud){
	struct heif_error err = { heif_error_Ok, (enum heif_suberror_code)0, NULL };
	luaL_Buffer* b = (luaL_Buffer*)ud;
	luaL_addlstring(b, (char*)data, size);
	return err;
}
static int exportHeif(lua_State* L, bool useAvif){
	const lil_Image* img = lil_getImage(L, 1);
	lil_Number quality, speed;
	lil_getExportOpts(L, 2, &quality, &speed);
	const bool lossLess = quality >= 0.9999 && quality <= 1.0001;
	const enum heif_compression_format format = (enum heif_compression_format)(lil_getOptEnum(L, 2, "format", formatsStr, useAvif ? "AV1" : "HEVC") + 1);
	const char* preset = lil_getOptStr(L, 2, "preset", NULL);
	const char* tune = lil_getOptStr(L, 2, "tune", NULL);

	struct heif_context* ctx = NULL;
	struct heif_encoder* encoder = NULL;
	struct heif_image* himg = NULL;
	struct heif_error err;
	const uint8_t* pixels;

	memset(&err, 0, sizeof(err));
	if((ctx = heif_context_alloc()) == NULL)
		goto fail;
	if((err = heif_context_get_encoder_for_format(ctx, format, &encoder)).code)
		goto fail;
	if((err = heif_encoder_set_lossy_quality(encoder, quality * 100 + 0.5)).code)
		goto fail;
	if((err = heif_encoder_set_lossless(encoder, lossLess)).code)
		goto fail;
	if(preset && format == heif_compression_HEVC)
		if((err = heif_encoder_set_parameter_string(encoder, "preset", preset)).code)
			goto fail;
	if(tune && format == heif_compression_HEVC)
		if((err = heif_encoder_set_parameter_string(encoder, "tune", tune)).code)
			goto fail;
	if((err = heif_image_create(img->w, img->h, heif_colorspace_RGB, heif_chroma_interleaved_RGBA, &himg)).code)
		goto fail;
	if((err = heif_image_add_plane(himg, heif_channel_interleaved, img->w, img->h, 32)).code)
		goto fail;

	int stride;
	pixels = heif_image_get_plane(himg, heif_channel_interleaved, &stride);
	if(!pixels)
		goto fail;

	if(stride == img->w * 4){
		memcpy((void*)pixels, img->dptr, (size_t)img->w * img->h * 4);
	} else {
		assert(0);
		goto fail;
	}

	if((err = heif_context_encode_image(ctx, himg, encoder, NULL, NULL)).code)
		goto fail;
	heif_encoder_release(encoder);
	heif_image_release(himg);

	luaL_Buffer b;
	luaL_buffinit(L, &b);
	struct heif_writer writer;
	writer.write = myHeifWriter;
	writer.writer_api_version = 1;
	if((err = heif_context_write(ctx, &writer, &b)).code)
		goto fail;

	heif_context_free(ctx);

	luaL_pushresult(&b);
	return 1;

	fail:
	lua_pushnil(L);
	if(err.code)
		lua_pushstring(L, err.message);
	else
		lua_pushstring(L, "Error encoding HEIF");

	heif_encoder_release(encoder);
	heif_image_release(himg);
	heif_context_free(ctx);
	return 2;
}
#ifndef LIL_USE_AVIF
LUAFUNC(exportHeifAvif){
	return exportHeif(L, true);
}
#endif
LUAFUNC(exportHeif){
	return exportHeif(L, false);
}

#endif

