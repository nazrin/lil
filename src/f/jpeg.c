/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#ifdef LIL_USE_JPEG
#include <turbojpeg.h>
#include <setjmp.h>

#include "../inc.h"

LUAFUNC(importJpeg){
	size_t len;
	const uint8_t* data = (const uint8_t*)lua_tolstring(L, 1, &len);
	tjhandle jpg = tjInitDecompress();
	if(!jpg)
		return 0;
	int w, h, jpegSubsamp;
	if(tjDecompressHeader2(jpg, (uint8_t*)data, len, &w, &h, &jpegSubsamp) == -1){
		tjDestroy(jpg);
		return 0;
	}
	lil_Image* img = lil_newImage(L, w, h);
	if(!img){
		tjDestroy(jpg);
		return 0;
	}
	if(tjDecompress2(jpg, (uint8_t*)data, len, img->dptr, w, 0, h, TJPF_RGBA, TJFLAG_FASTDCT) == -1){
		tjDestroy(jpg);
		lil_freeImage(img);
		return 0;
	}
	tjDestroy(jpg);
	lil_pushImage(L, img);
	return 1;
}

LUAFUNC(exportJpeg){
	const lil_Image* img = lil_getImage(L, 1);
	lil_Number quality;
	lil_getExportOpts(L, 2, &quality, NULL);
	quality = ((quality + 1) * 100) - 100;
	tjhandle jpg = tjInitCompress();
	if(!jpg)
		return 0;
	unsigned char* data = NULL;
	unsigned long len = 0;
	tjCompress2(jpg, img->dptr, img->w, 0, img->h, TJPF_RGBA, &data, &len, TJSAMP_444, quality, TJFLAG_FASTDCT);
	tjDestroy(jpg);
	lua_pushlstring(L, (const char*)data, len);
	tjFree(data);
	return 1;
}

#endif

