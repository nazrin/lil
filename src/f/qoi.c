/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#ifdef LIL_USE_QOI
#include <arpa/inet.h>

#include "../inc.h"

// https://qoiformat.org/qoi-specification.pdf
// https://en.wikipedia.org/wiki/QOI_(image_format)

enum ChunkTags{
	QOI_OP_RGB   = 0b11111110,
	QOI_OP_RGBA  = 0b11111111,
	QOI_OP_INDEX = 0b00000000,
	QOI_OP_DIFF  = 0b01000000,
	QOI_OP_LUMA  = 0b10000000,
	QOI_OP_RUN   = 0b11000000,
};

#pragma pack(1)
struct QoiHeader{
	char magic[4];
	uint32_t w;
	uint32_t h;
	uint8_t chans; // RGB = 3, RGBA = 4
	uint8_t cs; // sRGB = 0, linear = 1
} typedef QoiHeader;
#pragma pack()

static const uint8_t qoiFooter[8] = { 0, 0, 0, 0, 0, 0, 0, 1 };

#define qoiHash(col) (((col).r * 3 + (col).g * 5 + (col).b * 7 + (col).a * 11) % 64)

#define failExport() { \
	lil_buffer_free(&buf); \
	luaL_error(L, "Error encoding qoi"); \
	return 0; \
}
#define checkExportBufSize(s) { \
	if(lil_unlikely(lil_buffer_check(&buf, s))){ \
		if(lil_unlikely(lil_buffer_fit(&buf, s))) \
			failExport(); \
	} \
}
__attribute__((hot)) static int eat(const lil_Image* img, lua_State* L, const uint8_t** p, size_t* pleft){
	(*pleft)--;
	if(*pleft == 0){
		lil_freeImage(img);
		luaL_error(L, "Error qoi data too short");
	}
	return *((*p)++);
}
static lil_Colour trimPixel(lil_Colour p, uint8_t chans, bool strict){
	if(!(p.a || strict))
		p.c = 0;
	if(chans == 3)
		p.a = 0xff;
	return p;
}

LUAFUNC(importQoi){
	size_t len;
	const uint8_t* data = (const uint8_t*)lua_tolstring(L, 1, &len);
	if(len <= sizeof(QoiHeader))
		return 0;

	QoiHeader* header = (QoiHeader*)data;
	if(memcmp(header->magic, "qoif", 4) != 0)
		return 0;
	header->w = ntohl(header->w);
	header->h = ntohl(header->h);
	if(header->w == 0 || header->h == 0)
		return 0;
	if(header->chans != 3 && header->chans != 4)
		return 0;

	const lil_Image* img = lil_newImageCalloc(L, header->w, header->h);
	if(!img)
		return 0;

	const uint8_t* p = data + sizeof(QoiHeader);
	size_t pleft = len - sizeof(QoiHeader);
	lil_Colour prev = {{ 0, 0, 0, 255 }};
	lil_Colour seen[64] = { 0 };
	for(size_t o = 0; o < (size_t)img->w * img->h; o++){
		uint8_t c = eat(img, L, &p, &pleft);
		if(c == QOI_OP_RGB){
			img->d[o].r = eat(img, L, &p, &pleft);
			img->d[o].g = eat(img, L, &p, &pleft);
			img->d[o].b = eat(img, L, &p, &pleft);
			img->d[o].a = prev.a;
		} else if(c == QOI_OP_RGBA){
			img->d[o].r = eat(img, L, &p, &pleft);
			img->d[o].g = eat(img, L, &p, &pleft);
			img->d[o].b = eat(img, L, &p, &pleft);
			img->d[o].a = eat(img, L, &p, &pleft);
		} else {
			uint8_t m = c & 0b11000000;
			if(m == QOI_OP_INDEX){
				img->d[o] = seen[c & 0b00111111];
			} else if(m == QOI_OP_DIFF){
				img->d[o] = prev;
				img->d[o].r += ((c & 0b00110000) >> 4) - 2;
				img->d[o].g += ((c & 0b00001100) >> 2) - 2;
				img->d[o].b += ((c & 0b00000011) >> 0) - 2;
			} else if(m == QOI_OP_LUMA){
				uint8_t dg = (c & 0b00111111) - 32;
				uint8_t c2 = eat(img, L, &p, &pleft);
				img->d[o] = prev;
				img->d[o].r += dg + ((c2 & 0b11110000) >> 4) - 8;
				img->d[o].g += dg;
				img->d[o].b += dg + ((c2 & 0b00001111) >> 0) - 8;
			} else {
				img->d[o] = prev;
				for(int i = 0; i < (c & 0b00111111); i++){
					if(o+1 >= (size_t)img->w * img->h){
						lil_freeImage(img);
						return 0;
					}
					img->d[++o] = prev;
				}
			}
		}
		prev = img->d[o];
		seen[qoiHash(img->d[o])] = img->d[o];
	}

	lil_pushImage(L, img);
	return 1;
}

LUAFUNC(exportQoi){
	const lil_Image* img = lil_getImage(L, 1);
	const bool strict = lil_getOptBool(L, 2, "strict", false);

	QoiHeader header = {
		.magic = { 'q', 'o', 'i', 'f' },
		.w = htonl(img->w),
		.h = htonl(img->h),
		.chans = (uint8_t)(3 + (int)lil_getOptBool(L, 2, "alpha", true)),
		.cs = 0,
	};

	lil_Buffer buf;
	if(lil_unlikely(lil_buffer_init(&buf, (size_t)img->w*img->h*header.chans/2 + sizeof(header))))
		return 0;
	if(lil_unlikely(lil_buffer_addstr(&buf, (const char*)&header, sizeof(header))))
		return 0;

	lil_Colour prev = {{ 0, 0, 0, 255 }};
	lil_Colour seen[64] = { 0 };
	for(size_t o = 0; o < (size_t)img->w * img->h; o++){
		lil_Colour curr = trimPixel(img->d[o], header.chans, strict);
		short index = qoiHash(curr);
		if(curr.c == prev.c){ // Run length encoding
			size_t r = 1;
			while(o+r < (size_t)img->w * img->h && trimPixel(img->d[o+r], header.chans, strict).c == curr.c && r <= 61)
				r++;
			checkExportBufSize(1);
			buf.data[buf.size++] = QOI_OP_RUN | (r - 1);
			o += r - 1;
		} else {
			if(seen[index].c == curr.c){ // Index
				checkExportBufSize(1);
				buf.data[buf.size++] = QOI_OP_INDEX | index;
			} else {
				int8_t dr = curr.r - prev.r;
				int8_t dg = curr.g - prev.g;
				int8_t db = curr.b - prev.b;
				if(dr >= -2 && dr <= 1 && dg >= -2 && dg <= 1 && db >= -2 && db <= 1 && curr.a == prev.a){ // Difference
					checkExportBufSize(1);
					uint8_t d = (dr + 2) << 4 | (dg + 2) << 2 | (db + 2);
					buf.data[buf.size++] = QOI_OP_DIFF | d;
				} else {
					int8_t rdg = dr - dg;
					int8_t bdg = db - dg;
					if(dg >= -32 && dg <= 31 && rdg >= -8 && rdg <= 7 && bdg >= -8 && bdg <= 7 && curr.a == prev.a){ // Luma
						checkExportBufSize(2);
						buf.data[buf.size++] = QOI_OP_LUMA | (dg + 32);
						buf.data[buf.size++] = (rdg + 8) << 4 | (bdg + 8);
					} else { // RGB[A]
						if(curr.a == prev.a){ // RGB
							checkExportBufSize(4);
							buf.data[buf.size++] = QOI_OP_RGB;
							for(int i = 0; i < 3; i++)
								buf.data[buf.size++] = curr.arr[i];
						} else { // RGBA
							checkExportBufSize(5);
							buf.data[buf.size++] = QOI_OP_RGBA;
							for(int i = 0; i < 4; i++)
								buf.data[buf.size++] = curr.arr[i];
						}
					}
				}
			}
		}
		seen[index] = curr;
		prev = curr;
	}

	if(lil_unlikely(lil_buffer_addstr(&buf, (const char*)&qoiFooter, sizeof(qoiFooter))))
		return 0;

	lua_pushlstring(L, (const char*)buf.data, buf.size);
	lil_buffer_free(&buf);
	return 1;
}

#endif

