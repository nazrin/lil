/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#ifdef LIL_USE_WEBP
#include <webp/decode.h>
#include <webp/encode.h>

#include "../inc.h"

LUAFUNC(importWebp){
	size_t len;
	const uint8_t* data = (const uint8_t*)lua_tolstring(L, 1, &len);
	int w, h;
	WebPGetInfo(data, len, &w, &h);
	const lil_Image* img = lil_newImage(L, w, h);
	if(!img)
		return 0;
	WebPDecodeRGBAInto(data, len, img->dptr, w * h * sizeof(lil_Colour), w * sizeof(lil_Colour));
	lil_pushImage(L, img);
	return 1;
}

LUAFUNC(exportWebp){
	const lil_Image* img = lil_getImage(L, 1);
	uint8_t* data = NULL;
	size_t size = 0;

	lil_Number quality;
	lil_getExportOpts(L, 2, &quality, NULL);

	lua_pop(L, 1);
	if(quality >= 0.9999 && quality <= 1.0001)
		size = WebPEncodeLosslessRGBA(img->dptr, img->w, img->h, img->w * 4, &data);
	else  // Is this loss?
		size = WebPEncodeRGBA(img->dptr, img->w, img->h, img->w * 4, quality * 100.0f, &data);
	if(size == 0)
		return 0;
	lua_pushlstring(L, (const char*)data, size);
	WebPFree(data);
	return 1;
}

#endif

