/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#include <stdint.h>
#include <lua.h>

#include "config.h"

union lil_Colour{
	uint8_t arr[4];
	uint32_t c;
	uint32_t rgb: 24;
	struct{
		uint8_t r;
		uint8_t g;
		uint8_t b;
		uint8_t a;
	};
} typedef lil_Colour;

struct lil_Image{
	union{
		lil_Colour* lil_restrict d;
		uint8_t* lil_restrict dptr;
	};
	int w, h;
} typedef lil_Image;

lil_Image* lil_cloneImage(lua_State* L, const lil_Image* img);
lil_Image* lil_getImage(lua_State* L, const int i);
lil_Image* lil_newImage(lua_State* L, const int w, const int h);
lil_Image* lil_newImageCalloc(lua_State* L, const int w, const int h);
lil_Image** lil_getImagePointer(lua_State* L, const int i);
void lil_fillImage(const lil_Image* img, const lil_Colour col);
void lil_freeImage(const lil_Image* img);
void lil_pushImage(lua_State* L, const lil_Image* img);
void lil_setImageSize(lua_State* L, lil_Image* img);

