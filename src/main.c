/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
///
// @module Lil
// @license MPL-2.0
// @author nazrin
#include "inc.h"

#include "../aux_src.h"

static void appendToMetatable(lua_State* L, const struct luaL_Reg* lib){
	for(int i = 0; lib[i].func; i++){
		lua_pushcfunction(L, lib[i].func);
		lua_setfield(L, -2, lib[i].name);
	}
}
static void createMetatable(lua_State* L, const char* id, const struct luaL_Reg* lib){
	luaL_newmetatable(L, id);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	lua_pushstring(L, id);
	lua_setfield(L, -2, "__name");
	appendToMetatable(L, lib);
}

static void addHandler(lua_State* L, const char* id, const char* pattern, const char** extensions, lua_CFunction imp, lua_CFunction exp){
	lua_newtable(L);
	lua_pushcfunction(L, imp);
	lua_setfield(L, -2, "import");
	lua_pushcfunction(L, exp);
	lua_setfield(L, -2, "export");
	lua_pushstring(L, pattern);
	lua_setfield(L, -2, "pattern");
	int a;
	for(a = 0; extensions[a]; a++);
	lua_createtable(L, a, 0);
	for(int e = 0; e < a; e++){
		lua_pushstring(L, extensions[e]);
		lua_rawseti(L, -2, e + 1);
	}
	lua_setfield(L, -2, "extensions");
	lua_setfield(L, -2, id);
}

extern LUAREG(lilLib);
extern LUAREG(colourLib);
extern LUAREG(imgLib);
extern LUAREG(imgUdLib);
extern LUAREG(imgFilterLib);
extern LUAREG(imgDrawLib);
#ifdef LIL_USE_FREETYPE
extern LUAREG(fontUdLib);
#endif

#define DEF(id) LUAFUNC(import ## id); LUAFUNC(export ## id)
#ifdef LIL_USE_FARBFELD
	DEF(Farbfeld);
#endif
#ifdef LIL_USE_WEBP
	DEF(Webp);
#endif
#ifdef LIL_USE_PNG
	DEF(Png);
#endif
#ifdef LIL_USE_JPEG
	DEF(Jpeg);
#endif
#ifdef LIL_USE_GIF
	DEF(Gif);
#endif
#ifdef LIL_USE_QOI
	DEF(Qoi);
#endif
#ifdef LIL_USE_AVIF
	DEF(Avif);
#endif
#ifdef LIL_USE_HEIF
	DEF(Heif);
	#ifndef LIL_USE_AVIF
		LUAFUNC(exportHeifAvif);
	#endif
#endif
#undef DEF

static LUAFUNC(clean_gc){
	return 0;
}
static LUAREG(cleanLib) = {
	{ "__gc", LUAFUNCD(clean_gc) },
	{ NULL, NULL }
};

lil_externc int LUAOPEN_LIL(lua_State* L){
	createMetatable(L, LIL_MT,         LUAREGD(lilLib));
	createMetatable(L, LIL_IMG_MT,     LUAREGD(imgLib));
	createMetatable(L, LIL_CLEAN_MT,   LUAREGD(cleanLib));
	createMetatable(L, LIL_IMG_UD_MT,  LUAREGD(imgUdLib));
	#ifdef LIL_USE_FREETYPE
	createMetatable(L, LIL_FONT_UD_MT, LUAREGD(fontUdLib));
	#endif

	// Root library table
	lua_createtable(L, 0, 6);
	luaL_newmetatable(L, LIL_MT);
	appendToMetatable(L, LUAREGD(colourLib));
	lua_pushstring(L, LIL_MT);
	lua_setfield(L, -2, "__name");
	lua_setmetatable(L, -2);

	// Format handlers
	lua_createtable(L, 0, 3);

	#ifdef LIL_USE_FARBFELD
		const char* farbfeldPats[] = { "%.farbfeld$", "%.ff$", NULL };
		addHandler(L, "farbfeld", "^farbfeld", farbfeldPats, LUAFUNCD(importFarbfeld), LUAFUNCD(exportFarbfeld));
	#endif

	#ifdef LIL_USE_WEBP
		const char* webpPats[] = { "%.webp$", NULL };
		addHandler(L, "webp", "^RIFF....WEBP", webpPats, LUAFUNCD(importWebp), LUAFUNCD(exportWebp));
	#endif

	#ifdef LIL_USE_PNG
		const char* pngPats[] = { "%.png$", NULL };
		addHandler(L, "png", "^\x89PNG\x0d\x0a\x1a\x0a", pngPats, LUAFUNCD(importPng), LUAFUNCD(exportPng));
	#endif

	#ifdef LIL_USE_JPEG
		const char* jpegPats[] = { "%.jpe?g$", "%.jfif?$", "%.jpe$", "%.jif$", NULL };
		addHandler(L, "jpeg", "^\xff\xd8\xff", jpegPats, LUAFUNCD(importJpeg), LUAFUNCD(exportJpeg));
	#endif

	#ifdef LIL_USE_GIF
		const char* gifPats[] = { "%.gif$", NULL };
		addHandler(L, "gif", "^GIF8.a", gifPats, LUAFUNCD(importGif), LUAFUNCD(exportGif));
	#endif

	#ifdef LIL_USE_QOI
		const char* qoiPats[] = { "%.qoi$", NULL };
		addHandler(L, "qoi", "^qoif", qoiPats, LUAFUNCD(importQoi), LUAFUNCD(exportQoi));
	#endif

	#ifdef LIL_USE_AVIF
		const char* avifPats[] = { "%.avif$", NULL };
		addHandler(L, "avif", "^....ftypavif", avifPats, LUAFUNCD(importAvif), LUAFUNCD(exportAvif));
	#endif

	#ifdef LIL_USE_HEIF
		const char* heifPats[] = { "%.heif$", "%.heic$", NULL };
		addHandler(L, "heif", "^....ftypheic", heifPats, LUAFUNCD(importHeif), LUAFUNCD(exportHeif));
		#ifndef LIL_USE_AVIF
			const char* avifPats[] = { "%.avif$", NULL };
			addHandler(L, "avif", "^....ftypavif", avifPats, LUAFUNCD(importHeif), LUAFUNCD(exportHeifAvif));
		#endif
	#endif

	lua_setfield(L, -2, "formatHandlers");

	// Metatables
	luaL_getmetatable(L, LIL_IMG_MT);
	appendToMetatable(L, LUAREGD(imgFilterLib));
	appendToMetatable(L, LUAREGD(imgDrawLib));
	lua_setfield(L, -2, "imgMT");

	// Metafields
	#ifdef LIL_VERSION
	lua_pushstring(L, LIL_VERSION);
	lua_setfield(L, -2, "_VERSION");
	#endif
	lua_pushstring(L, "MPL-2.0");
	lua_setfield(L, -2, "_LICENSE");


	// Load aux
	if(luaL_dostring(L, aux_src))
		lua_error(L);
	lua_pushvalue(L, -2);
	lua_call(L, 1, 0);

	// Cleanup UD
	lua_newuserdata(L, 1);
	luaL_getmetatable(L, LIL_CLEAN_MT);
	lua_setmetatable(L, -2);
	lua_setfield(L, -2, "__clean");

	return 1;
}

