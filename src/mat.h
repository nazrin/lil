/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

struct lil_Mat{
	int w, h;
	lil_Number* d;
} typedef lil_Mat;

void lil_mat_free(lil_Mat* mat);
lil_Mat lil_mat_get(lua_State* L, int i);
void lil_mat_dot(const lil_Mat* mat1, const lil_Mat* mat2, lil_Mat* res);
void lil_mat_init(lua_State* L, lil_Mat* mat);
int lil_mat_resize(lil_Mat* mat, int w, int h);

