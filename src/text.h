/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once
#ifdef LIL_USE_FREETYPE
#include <freetype2/ft2build.h>
#include <fontconfig/fontconfig.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

struct lil_Font {
	FT_Face face;
	lil_Colour colour;
	int w, h;
	bool vector;
} typedef lil_Font;

wchar_t* lil_utf8ToWchar(const unsigned char* const text, const size_t textLen, size_t* const lenRet);
lil_Font* lil_newFont(const char* path, int w, int h, lil_Colour c);
int lil_initFreetype(void);
void lil_freeFreetype(void);
lil_Font* lil_getFont(lua_State* L, int i);
void lil_writeTextToImage(const lil_Font* font, const wchar_t* wtext, size_t len, int neededWidth, int neededHeight, const lil_Image* img, enum lil_BlendMode bm, int imgOffsetX, int imgOffsetY);
char* lil_findFontPath(const char* pat);

#endif

