/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
#pragma once

#define LUAFUNCD(id)                     lil_l_ ## id
#define LUAFUNC(id)                  int lil_l_ ## id (lua_State* L)
#define LUAREGD(id)                      l_lil_ ## id
#define LUAREG(id) const struct luaL_Reg l_lil_ ## id[]

#define PI 3.14159265358979323846

#define min(a, b)        ((a) > (b) ? (b) : (a))
#define max(a, b)        ((a) > (b) ? (a) : (b))
#define clamp(n, mi, ma) (min(max((n), mi), ma))
#define clamp255(n)      (clamp((n), 0, 255))
#define clamp1(n)        (clamp((n), 0, 1))
#define degToRad(deg)    ((deg) * (PI/180.))


#ifdef _OPENMP
#define TIMERSTART double tImErStart = omp_get_wtime()
#if defined(__clang__)
#define TIMEREND { double tImErEnd   = omp_get_wtime(); fprintf(stderr, "Clang TIMER %s() : %f\n", __func__, tImErEnd - tImErStart); }
#elif defined(__GNUC__) || defined(__GNUG__)
#define TIMEREND { double tImErEnd   = omp_get_wtime(); fprintf(stderr, "GCC TIMER %s() : %f\n", __func__, tImErEnd - tImErStart); }
#else
#define TIMEREND { double tImErEnd   = omp_get_wtime(); fprintf(stderr, "TIMER %s() : %f\n", __func__, tImErEnd - tImErStart); }
#endif
#else
#define TIMERSTART
#define TIMEREND fprintf(stderr, "TIMER %s(): not using openmp\n", __func__);
#endif

#if LUA_VERSION_NUM >= 502
#define lua_objlen lua_rawlen
#endif

#define lil_likely(x) (__builtin_expect(((x) != 0), 1))
#define lil_unlikely(x) (__builtin_expect(((x) != 0), 0))

#define isByte1(flags, i) ((flags & (1 << (i))) >> (i))
#define isWithinBounds(img, x, y) ((x) >= 0 && (y) >= 0 && (x) < (img)->w && (y) < (img)->h)

#define XY(img, x, y) (((img)->w * (y)) + (x))

void* lil_malloc(size_t s);
void* lil_realloc(void* ptr, size_t s);
void* lil_calloc(size_t n, size_t s);
void lil_free(void* ptr);
void lil_setMemFuncs(void* (*m)(size_t), void* (*r)(void*, size_t), void* (*c)(size_t, size_t), void (*f)(void*));

struct lil_Buffer{
	size_t size;
	size_t maxSize;
	uint8_t* data;
} typedef lil_Buffer;
#define lil_buffer_check(b, s) (((b)->maxSize - (b)->size) < (s))
int lil_buffer_init(lil_Buffer* b, size_t maxSize);
void lil_buffer_free(lil_Buffer* b);
int lil_buffer_fit(lil_Buffer* b, size_t size);
int lil_buffer_addstr(lil_Buffer* b, const char* str, size_t size);

void lil_dumpstack(lua_State* L);
lil_Colour lil_getColour(lua_State* L, int index, lil_Colour* def);
uintmax_t lil_getStringFlags(lua_State* L, int index, const char* list, const char* def);

enum lil_BlendMode lil_getOptBlendMode(lua_State* L, int i);
enum lil_SampleMode lil_getOptSampleMode(lua_State* L, int i, lil_Number xs, lil_Number ys);
int lil_getOptEnum(lua_State* L, int i, const char* key, const char* const* list, const char* const def);
int lil_getOptInt(lua_State* L, int i, const char* key, int def);
lil_Number lil_getOptFloat(lua_State* L, int i, const char* key, lil_Number def);
void lil_getOpt2dVec(lua_State* L, int i, const char* key, int* r1, int* r2);
bool lil_getOptBool(lua_State* L, int i, const char* key, int def);
void lil_getExportOpts(lua_State* L, int i, lil_Number* qualityRet, lil_Number* speedRet);
void lil_getOptRect(lua_State* L, int i, const char* key, int* xr, int* yr, int* wr, int* hr);
void lil_getOptColour(lua_State* L, int i, const char* key, lil_Colour* c);
const char* lil_getOptStr(lua_State* L, int i, const char* key, const char* def);

int lil_constrainRectImg(lua_State* L, const lil_Image* bot, int* bx, int* by, const lil_Image* top, int* tx, int* ty, int* w, int* h);
int lil_constrainRect(lua_State* L, const lil_Image* bot, int* bx, int* by, int* w, int* h);

