-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.
local ifile = io.stdin
local ofile = io.stdout

ofile:write("#pragma once\n")
ofile:write(("/* Generated %s */\n"):format(os.date("%a %b %d %Y %H:%M:%S")))
ofile:write(("const char* %s = \""):format(assert(arg[1])))

for l in ifile:lines() do
	l = l:gsub("^\t*", "")
	if l:find("^%s*%-%-") or #l == 0 then
		goto skip
	end
	for i=1,#l do
		local b = l:byte(i)
		if b >= 0x20 and b <= 0x7f and b ~= 0x5c and b ~= 0x22 then
			ofile:write(string.char(b))
		else
			ofile:write(("\\%03o"):format(b))
		end
	end
	ofile:write("\\n")
	::skip::
end

ofile:write("\";\n\n")

