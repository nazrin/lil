-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.
package.cpath = "./?.so"
local lil = require("lil")
require("doc.examples.lil-ffbz2")(lil)

local fontPath = ""

local function floatMatch(f1, f2)
	return f1 + 0.01 > f2 and f2 + 0.01 > f1
end
local function tableMatch(t1, t2)
	for k,v in ipairs(t1, t2) do
		if type(v) == "number" then
			if not floatMatch(v, t2[k]) then
				return false
			end
		else
			if v ~= t2[k] then
				return false
			end
		end
	end
	return true
end
local function gradient(img)
	img:map(function(r, g, b, a, x, y)
		return x / img.w, x / y, y / img.h, (x * y) / (img.w * img.h)
	end)
end
local function shouldFail(func, ...)
	local ok, err =  pcall(func, ...)
	assert(not ok)
end

local function funcaA(img)
	local function func1(x, r, g, b, a)
		img:fill(x)
		local c = { img:getPixel(0, 0) }
		assert(tableMatch, { r, g, b, a })
	end
	func1("fff", 1, 1, 1, 1)
	func1("#fff", 1, 1, 1, 1)
	func1("fff0", 0, 0, 0, 0)
	func1("ffff", 1, 1, 1, 1)
	func1("ff00ff", 1, 0, 1, 1)
	func1("00ff00ff", 0, 1, 0, 1)
	func1({ 1, 1, 1, 1 }, 1, 1, 1, 1)
	func1({ 1, 1, 1 }, 1, 1, 1, 1)
	func1({ 0.0, 0.0, 0.0 }, 0.0, 0.0, 0.0, 1)

	local function func2(fmt, loss)
		gradient(img)
		local data = assert(img:export(fmt))
		local img2 = assert(lil.import(data))
		assert(img.w == img2.w and img.h == img2.h)
	end
	func2(".png", false)
	func2(".qoi", false)
	func2(".farbfeld", false)
	func2(".webp", false)
	func2(".jpg", true)

	gradient(img)
	local function func3()
		local font = assert(lil.font(fontPath, "000", 25))
		local fimg1 = font:text("sæll, \r\nпроверю,\r Ω こんにちは")
		local fimg2 = font:text("s\ræll, \nпроверю, \rΩ こんにちは")
		assert(fimg1 == fimg2)
		local clone = img:clone()
		assert(clone == img)
		clone:fill("0000")
		clone:comp(img, 0, 0, { blend = "replace" })
		assert(clone == img)
		clone:fill("1234")
		assert(clone ~= img)
		assert(not clone:isOpaque())
		clone:fill("abc")
		assert(clone:isOpaque())
	end
	func3()

	gradient(img)
	local function func4()
		img:fill("fff")
		img:rect("000", 0, 0, img.w, img.h, { blend = "multiply" })
		assert(img:getPixel(0, 0) == 0)
		local w, h = img.w, img.h
		img:resize(50, 50)
		assert(img:getPixel(0, 0) == 0)
		img:resize(w, h)
		assert(img:getPixel(0, 0) == 0)
		img:setPixel(1, 0, 1, 1, 0, 0)
		assert(tableMatch({ img:getPixel(0, 0) }, { 1, 0, 1, 1 }))
	end
	func4()

	gradient(img)
	local function func5()
		img:map(function()
			return 0, 1, 1, 1
		end)
		assert(tableMatch({ img:getPixel(0, 0) }, { 0, 1, 1, 1 }))
		img:contrast(-1)
		assert(tableMatch({ img:getPixel(0, 0) }, { 0.5, 0.5, 0.5, 1 }))
		img:fill("fff"):invert()
		assert(tableMatch({ img:getPixel(0, 0) }, { 0, 0, 0, 1 }))
		img:gamma(5)
		img:brightness(1)
		assert(tableMatch({ img:getPixel(0, 0) }, { 1, 1, 1, 1 }))
		img:fill("abc"):grey()
		local r, g, b = img:getPixel(img.w - 1, img.h - 1)
		assert(r == g and g == b)
		img:fill("abc"):bw()
		r, g, b = img:getPixel(0, 0)
		assert(r == g and g == b)
		assert(r == 0 or r == 1)
		assert(img:clone() == img:kernel({{ 1 }}))
		assert(img:clone() == img:kernel({{ 1 }, { 1 }}))
	end
	func5()
	
	gradient(img)
	local function func5()
		img:rect("aff", 50, 50, 50, 50)
		img:rect("ffa", 5, 5, 5, 5)
		img:rect("faf", 5, 5, 1000, 5)
		img:circle("0ff", 50, 50, 50)
		img:circle("ff0", 5, 5, 5)
		img:circle("f0f", 5, 5, 1000)
		img:circle("a0f", 5, 5, 1)
	end
	func5()

	gradient(img)
	local function func6()
		local w, h = img.w, img.h
		img:resize(1, 1)
		img:resize(w, h)
	end
	func6()

	gradient(img)
	local function func7()
		img:clone():crop(0, 0, 1, 1)
		img:clone():resize(1, 1)
		img:flip()
		img:rotate(180)
	end
	func7()
end

local img = lil.new(5, 5)
assert(img and img.w == 5 and img.h == 5)
funcaA(img)

funcaA(lil.new(50, 500))
funcaA(lil.new(501, 501))
funcaA(lil.new(1, 501))
funcaA(lil.new(1, 1))
funcaA(lil.new(2, 1))
funcaA(lil.new(2, 3))
funcaA(lil.new(500, 1))

local function funcb(lib)
	for k,v in pairs(lib) do
		if type(v) == "function" then
			shouldFail(v)
		end
	end
end
funcb(lil)
funcb(img)

shouldFail(function() lil.new(0, 0) end)
shouldFail(function() lil.new(-1, 0) end)
shouldFail(function() lil.new(-1, -1) end)

shouldFail(function() img.rect(-1, 0, 1, 1) end)
shouldFail(function() img.rect(1, 0, 1, 1) end)
shouldFail(function() img.rect(0, 0, 1, 1) end)
shouldFail(function() img.rect(1, 1, 0, 1) end)
shouldFail(function() img.rect(1, 1, 1, 2) end)

shouldFail(function() img.crop(-1, -1, -1, -1) end)
shouldFail(function() img.crop(1, 1, -1, -1) end)
shouldFail(function() img.crop(-1, -1, 1, 1) end)
shouldFail(function() img.crop(0, 0, 0, 0) end)

shouldFail(function() lil.import("\0") end)
shouldFail(function() lil.import("") end)
shouldFail(function() lil.import("aáb") end)
shouldFail(function() lil.handlers.png.import("\0") end)
shouldFail(function() lil.handlers.png.import("") end)
shouldFail(function() lil.handlers.png.import("aáb") end)
shouldFail(function() img.kernel({}) end)
shouldFail(function() img.kernel({{}}) end)
shouldFail(function() img.kernel({{1},{1,1}}) end)

